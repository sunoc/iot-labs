#!/usr/bin/python3

import socket, sys
from knxnet import *
from sys import argv

gateway_ip = '127.0.0.1' # localhost IP is for the simulator. Replace with the
                         # real IP for the physical gateway
gateway_port = 3671      # default for both the simualtor and the physical
                         # gateway
                        

# These values are the for the simulator -- mind that the port *differs* from
# the gateway's! Replace both with ('0.0.0.0', 0) in a NAT-based VLAN
# (physical deployment)
data_endpoint   = ('127.0.0.1', 3672)
control_enpoint = ('127.0.0.1', 3672)
#data_endpoint   = ('0.0.0.0', 0)
#tunnetrol_enpoint = ('0.0.0.0', 0)

# -> Socket creation
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#sock.bind(('', gateway_port))
    
def knx_client ( GROUP_ADDRESS, PAYLOAD ):

    print ('Group Address:', GROUP_ADDRESS, type(GROUP_ADDRESS))
    dest_addr_group=knxnet.GroupAddress.from_str(GROUP_ADDRESS)
    print ('Payload:', PAYLOAD, type(PAYLOAD))



    ### Connection request
    # -> Sending Connection request
    conn_req_object = knxnet.create_frame(
        knxnet.ServiceTypeDescriptor.CONNECTION_REQUEST,
        control_enpoint,
        data_endpoint
    )
    conn_req_dtgrm = conn_req_object.frame   # -> Serializing
    sock.sendto(
        conn_req_dtgrm,
        (gateway_ip, gateway_port)
    )

    # <- Receiving Connection response
    data_recv, addr = sock.recvfrom(1024)
    conn_resp_object = knxnet.decode_frame(data_recv)

    
    # <- Retrieving channel_id from Connection response
    conn_channel_id = conn_resp_object.channel_id
    if conn_resp_object.status != 0:
        print ('Connection error wth status:', conn_resp_object.status)

    print ('Channel ID:', conn_channel_id)

    ### Connection state request
    # -> Sending Connection State request
    conn_state_object = knxnet.create_frame(
        knxnet.ServiceTypeDescriptor.CONNECTION_STATE_REQUEST,
        conn_channel_id,
        control_enpoint
    )
    conn_state_dtgrm = conn_state_object.frame   # -> Serializing
    sock.sendto(
        conn_state_dtgrm,
        (gateway_ip, gateway_port)
    )

    # <- Receiving Connection state response
    data_recv, addr = sock.recvfrom(1024)
    conn_state_object = knxnet.decode_frame(data_recv)
    if conn_state_object.channel_id  != conn_channel_id:
        print ('Error: unmatching channel IDs')
    if conn_state_object.status != 0:
        print ('Connection error wth status:', conn_state_object.status)

    ### Tunneling request
    # -> Sending tunneling request
    tunn_req_object = knxnet.create_frame(
        knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
        dest_addr_group,
        conn_channel_id,
        PAYLOAD[0],
        PAYLOAD[1],
        PAYLOAD[2]
    )
    tunn_req_dtgrm = tunn_req_object.frame # -> Serializing
    sock.sendto(
        tunn_req_dtgrm,
        (gateway_ip, gateway_port)
    )

    # <- Receiving tunneling ack
    data_recv, addr = sock.recvfrom(1024)
    tunn_ack_object = knxnet.decode_frame(data_recv)
    if tunn_ack_object.channel_id  != conn_channel_id:
        print ('Error: unmatching channel IDs')
    if tunn_ack_object.status != 0:
        print ('Connection error wth status:', conn_state_object.status)


    ### Deconnection request
    # -> Sending Deconnection request
    deconn_req_object = knxnet.create_frame(
        knxnet.ServiceTypeDescriptor.DISCONNECT_REQUEST,
        conn_channel_id,
        control_enpoint
    )
    deconn_req_dtgrm = deconn_req_object.frame   # -> Serializing
    sock.sendto(
        deconn_req_dtgrm,
        (gateway_ip, gateway_port)
    )

    # <- Receiving Connection response
    data_recv, addr = sock.recvfrom(1024)
    deconn_resp_object = knxnet.decode_frame(data_recv)

    
    # <- Retrieving channel_id from Connection response
    data_recv, addr = sock.recvfrom(1024)
    deconn_req_object = knxnet.decode_frame(data_recv)
    if deconn_req_object.channel_id  != conn_channel_id:
        print ('Error: unmatching channel IDs')
    if deconn_req_object.status != 0:
        print ('Connection error wth status:', deconn_req_object.status)
    
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")
knx_client (argv[1], [int(argv[2]), int(argv[3]), int(argv[4])])
